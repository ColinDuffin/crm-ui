import * as types from './actionTypes';
import { beginApiCall, apiCallError } from './apiStatusActions';
import * as interactionAxios from '../../api/interactionAxios';

export function loadInteractionSuccess(interactions) {
  return { type: types.LOAD_INTERACTIONS_SUCCESS, interactions };
}

export function loadInteractions() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return interactionAxios
      .getInteractions()
      .then((interactions) => {
        dispatch(loadInteractionSuccess(interactions));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}
