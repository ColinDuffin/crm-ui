import React from 'react';
import { cleanup, render } from 'react-testing-library';
import ManageCustomerForm from '../ManageCustomerForm';

afterEach(cleanup);

function renderManageCustomerForm(args) {
  let defaultProps = {
    interactions: [],
    customer: {},
    saving: false,
    errors: {},
    onSave: () => {},
    onChange: () => {},
  };

  const props = { ...defaultProps, ...args };
  return render(<ManageCustomerForm {...props} />);
}

it('should render Add Course header', () => {
  const { getByText } = renderManageCustomerForm();
  getByText('Add Customer');
});

it('should label save button as "Save" when not saving', () => {
  const { getByText } = renderManageCustomerForm();
  getByText('Save');
});

it('should label save button as "Saving..." when saving', () => {
  const { getByText, debug } = renderManageCustomerForm({ saving: true });
  // debug();
  getByText('Saving...');
});
