import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function interactionReducer(
  state = initialState.interactions,
  action
) {
  switch (action.type) {
    case types.LOAD_INTERACTIONS_SUCCESS:
      return action.interactions;
    default:
      return state;
  }
}
