import customerReducer from '../customerReducer';
import * as actions from '../../actions/customerActions';

it('should add customer when passed CREATE_CUSTOMER_SUCCESS', () => {
  // arrange
  const initialState = [
    {
      fname: 'A',
    },
    {
      fname: 'B',
    },
  ];

  const newCustomer = {
    fname: 'C',
  };

  const action = actions.createCustomerSuccess(newCustomer);

  // act
  const newState = customerReducer(initialState, action);

  // assert
  expect(newState.length).toEqual(3);
  expect(newState[0].fname).toEqual('A');
  expect(newState[1].fname).toEqual('B');
  expect(newState[2].fname).toEqual('C');
});

it('should update customer when passed UPDATE_CUSTOMER_SUCCESS', () => {
  // arrange
  const initialState = [
    { id: 1, fname: 'A' },
    { id: 2, fname: 'B' },
    { id: 3, fname: 'C' },
  ];

  const customer = { id: 2, fname: 'New Name' };
  const action = actions.updateCustomerSuccess(customer);

  // act
  const newState = customerReducer(initialState, action);
  const updatedCustomer = newState.find((a) => a.id == customer.id);
  const untouchedCustomer = newState.find((a) => a.id == 1);

  // assert
  expect(updatedCustomer.fname).toEqual('New Name');
  expect(untouchedCustomer.fname).toEqual('A');
  expect(newState.length).toEqual(3);
});
