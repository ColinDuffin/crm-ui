import React from 'react';
import ManageCustomerForm from '../ManageCustomerForm';
import { shallow } from 'enzyme';

function renderManageCustomerForm(args) {
  const defaultProps = {
    interactions: [],
    customer: {},
    saving: false,
    errors: {},
    onSave: () => {},
    onChange: () => {},
  };

  const props = { ...defaultProps, ...args };
  return shallow(<ManageCustomerForm {...props} />);
}

it('renders the form + the header', () => {
  const wrapper = renderManageCustomerForm();

  expect(wrapper.find('form').length).toBe(1);
  expect(wrapper.find('h2').text()).toEqual('Add Customer');
});

it('labels save buttons as "Save" when not saving', () => {
  const wrapper = renderManageCustomerForm();
  expect(wrapper.find('button').text()).toBe('Save');
});

it('labels save button as "Saving..." when saving', () => {
  const wrapper = renderManageCustomerForm({ saving: true });
  expect(wrapper.find('button').text()).toBe('Saving...');
});
