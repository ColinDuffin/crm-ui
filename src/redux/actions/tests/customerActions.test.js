import * as customerActions from '../customerActions';
import * as types from '../actionTypes';
import { customers } from '../../../../tools/mockData-cust';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);
const axiosMock = new MockAdapter(axios);

// describe('Async actaions', () => {
//   afterEach(() => {
//     axiosMock.restore();
//   });

//   describe('Load Customers Thunk', () => {
//     it('should create BEGIN_API_CALL and LOAD_CUSTOMERS_SUCCESS when loading customers', () => {
//       axiosMock.get('*', {
//         body: customers,
//         headers: { 'content-type': 'application/json' },
//       });

//       const expectedActions = [
//         { type: types.BEGIN_API_CALL },
//         { type: types.LOAD_CUSTOMERS_SUCCESS, customers },
//       ];

//       const store = mockStore({ customers: [] });
//       return store.dispatch(customerActions.loadCustomers()).then(() => {
//         expect(store.getActions()).toEqual(expectedActions);
//       });
//     });
//   });
// });

describe('create course success', () => {
  it('should create a create customer success action', () => {
    const customer = customers[0];
    const expectedAction = {
      type: types.CREATE_CUSTOMER_SUCCESS,
      customer,
    };

    const action = customerActions.createCustomerSuccess(customer);

    expect(action).toEqual(expectedAction);
  });
});
