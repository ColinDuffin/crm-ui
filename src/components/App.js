import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "./home/HomePage";
import AboutPage from "./about/AboutPage";
import SettingsPage from "./settings/Settings";
import ReportsPage from "./reports/Reports";
import Header from "./common/Header";
import PageNotFound from "./PageNotFound";
import CustomersPage from "./customers/CustomersPage";
import ManageCustomerPage from "./customers/ManageCustomerPage"; //eslint-disable-line import/no-named-as-default
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="container-fluid">
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/customers" component={CustomersPage} />
        <Route path="/customer/:id" component={ManageCustomerPage} />
        <Route path="/customer/" component={ManageCustomerPage} />
        <Route path="/settings/" component={SettingsPage} />
        <Route path="/reports/" component={ReportsPage} />
        <Route component={PageNotFound} />
      </Switch>
      <ToastContainer autoClose={3000} hideProgressBar />
    </div>
  );
}

export default App;
