import React from 'react';
import { mount } from 'enzyme';
import {
  interactions,
  newCustomer,
  customers,
} from '../../../../tools/mockData-cust';
import { ManageCustomerPage } from '../ManageCustomerPage';

function render(args) {
  const defaultProps = {
    interactions,
    customers,
    // Passed from React Router in real app, so just stubbing in for test.
    // Could also choose to use MemoryRouter as shown in Header.test.js,
    // or even wrap with React Router, depending on whether I
    // need to test React Router related behavior.
    history: {},
    saveCustomer: jest.fn(),
    loadCustomers: jest.fn(),
    loadInteractions: jest.fn(),
    customer: newCustomer,
    match: {},
  };

  const props = { ...defaultProps, ...args };

  return mount(<ManageCustomerPage {...props} />);
}

it('sets error when attempting to save an empty First Name field', () => {
  const wrapper = render();
  wrapper.find('form').simulate('submit');
  const error = wrapper.find('.alert').first();
  expect(error.text()).toBe('First Name is required');
});
