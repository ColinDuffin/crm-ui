import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const CustomerList = ({ customers, onDeleteClick }) => (
  <div className="table-responsive">
    <table className="table table-striped table-sm table-hover">
      <thead className="thead-dark">
        <tr>
          <th />
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Status</th>
          <th>Linked Agent</th>
          <th />
          <th />
        </tr>
      </thead>
      <tbody>
        {customers.map((customer, index) => {
          return (
            <tr key={index}>
              <td></td>
              <td>{customer.id}</td>
              <td>{customer.fname}</td>
              <td>{customer.lname}</td>
              <td>{customer.status}</td>
              <td>{customer.linkedAgent}</td>
              <td>
                <button
                  className="btn btn-danger float-right"
                  onClick={() => onDeleteClick(customer)}
                >
                  <i className="fas fa-user-minus"></i>
                </button>
              </td>
              <td>
                <Link to={"/customer/" + customer.id}>
                  <button className="btn btn-info float-right">
                    <i className="fas fa-address-card"></i>
                  </button>
                </Link>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>
);

CustomerList.propTypes = {
  customers: PropTypes.array.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
};

export default CustomerList;
