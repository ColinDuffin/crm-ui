import { createStore } from 'redux';
import rootReducer from '../reducers';
import initialState from '../reducers/initialState';
import * as customerActions from '../actions/customerActions';

it('Should handle creating customers', function () {
  // arrange
  const store = createStore(rootReducer, initialState);
  const customer = {
    fname: 'Testing',
  };

  // act
  const action = customerActions.createCustomerSuccess(customer);
  store.dispatch(action);

  // assert
  const createdCustomer = store.getState().customers[0];
  expect(createdCustomer).toEqual(customer);
});
