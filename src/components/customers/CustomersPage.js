import React from "react";
import { connect } from "react-redux";
import * as customerActions from "../../redux/actions/customerActions";
import * as interactionActions from "../../redux/actions/interactionActions";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import CustomersList from "./CustomersList";
import { Redirect } from "react-router-dom";
import Spinner from "../common/Spinner";
import { toast } from "react-toastify";

// ** Container for Customer List **
class CustomersPage extends React.Component {
  state = {
    redirectToAddCustomerPage: false,
  };

  componentDidMount() {
    const { customers, actions } = this.props;

    //if (customers.length === 0) { // Having to API on each load of the page to force a reload after customer update.  what to check to avoid this?
    actions.loadCustomers().catch((error) => {
      alert("Loading customers failed" + error);
    });
    //}

    // if (interactions.length === 0) {
    actions.loadInteractions().catch((error) => {
      alert("Loading interactions failed" + error);
    });
    // }
  }

  handleDeleteCustomer = (customer) => {
    toast.success("Customer Deleted");
    this.props.actions.deleteCustomer(customer).catch((error) => {
      toast.error("Delete Failed. " + error.message, { autoClose: false });
    });
  };

  render() {
    return (
      <>
        {this.state.redirectToAddCustomerPage && <Redirect to="/customer" />}
        <h2>Customers</h2>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <>
            <CustomersList
              customers={this.props.customers}
              onDeleteClick={this.handleDeleteCustomer}
            />
            <button
              className="btn btn-primary float-right"
              onClick={() => this.setState({ redirectToAddCustomerPage: true })}
            >
              <i className="fa fa-user-plus"></i>
            </button>
          </>
        )}
      </>
    );
  }
}

CustomersPage.propTypes = {
  customers: PropTypes.array.isRequired,
  interactions: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    customers: state.customers,
    interactions: state.interactions,
    loading: state.apiCallsInProgress > 0,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadCustomers: bindActionCreators(
        customerActions.loadCustomers,
        dispatch
      ),
      loadInteractions: bindActionCreators(
        interactionActions.loadInteractions,
        dispatch
      ),
      deleteCustomer: bindActionCreators(
        customerActions.deleteCustomer,
        dispatch
      ),
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersPage);
