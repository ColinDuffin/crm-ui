const customers = [
  {
    id: 1,
    fname: "Ann",
    lname: "Elk",
    addressLine: "100 New Town Street",
    city: "New Town",
    zip: "60090",
    phone: "848-8484-848",
    email: "ann.elk@gmail.com",
    dnsFlag: false,
    status: "Active",
    linkedAgent: "Pierce Doyle",
  },
  {
    id: 2,
    fname: "Henry",
    lname: "Eighth",
    addressLine: "94 Big Street",
    city: "Green City",
    zip: "90455",
    phone: "917-1284-215",
    email: "henry.8@yahoo.com",
    dnsFlag: false,
    status: "Active",
    linkedAgent: "Lexie Ellis",
  },
];

const interactions = [
  {
    id: 1,
    custId: "1",
    activityCode: "1001",
    activityDesc: "Account Created",
    timestamp: "2020-08-08T18:20:00+00:00",
  },
  {
    id: 2,
    custId: "2",
    activityCode: "1001",
    activityDesc: "Account Created",
    timestamp: "2020-09-01T18:20:00+00:00",
  },
  {
    id: 3,
    custId: "2",
    activityCode: "1002",
    activityDesc: "Email Updated from henry.8@gmail.com to henry.8@gmail.com",
    timestamp: "2020-09-02T18:20:00+00:00",
  },
  {
    id: 4,
    custId: "1",
    activityCode: "1003",
    activityDesc: "Phone Number Updated from 555-8484-848 to 848-8484-848",
    timestamp: "2020-08-08T18:20:00+00:00",
  },
];

const newCustomer = {
  id: null,
  fname: "",
  lname: "",
  addressLine: "",
  city: "",
  zip: "",
  phone: "",
  email: "",
  dnaFlag: null,
  status: "",
  linkedAgent: "",
};

// Using CommonJS style export so we can consume via Node (without using Babel-node)
module.exports = {
  newCustomer,
  customers,
  interactions,
};
