import * as types from './actionTypes';
import * as customerAxios from '../../api/customerAxios';
import { beginApiCall, apiCallError } from './apiStatusActions';

export function createCustomerSuccess(customer) {
  return { type: types.CREATE_CUSTOMER_SUCCESS, customer };
}

export function updateCustomerSuccess(customer) {
  return { type: types.UPDATE_CUSTOMER_SUCCESS, customer };
}

export function loadCustomersSuccess(customers) {
  return { type: types.LOAD_CUSTOMERS_SUCCESS, customers };
}

export function deleteCustomerOptimistic(customer) {
  return { type: types.DELETE_CUSTOMER_OPTIMSITIC, customer };
}

export function loadCustomers() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return customerAxios
      .getCustomers()

      .then((customers) => {
        dispatch(loadCustomersSuccess(customers));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}

export function saveCustomer(customer) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return customerAxios
      .saveCustomer(customer)
      .then((savedCustomer) => {
        customer.custId
          ? dispatch(updateCustomerSuccess(savedCustomer))
          : dispatch(createCustomerSuccess(savedCustomer));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}

export function deleteCustomer(customer) {
  return function (dispatch) {
    dispatch(deleteCustomerOptimistic(customer));
    return customerAxios.deleteCustomer(customer.id);
  };
}
