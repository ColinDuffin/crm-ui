import React from "react";
import { Link } from "react-router-dom";

const HomePage = () => (
  <div className="container">
    <div className="jumbotron text-white jumbotron-image shadow">
      <h4>Acme Corporation</h4>
      <h1>Customer Relationship Managment</h1>
      <p>
        <small>IOU: One Authorization Component</small>
      </p>
      <Link to="customers" className="btn btn-primary btn-lg">
        Log In
      </Link>
    </div>
  </div>
);

export default HomePage;
