import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import * as customerActions from '../../redux/actions/customerActions';
import * as interactionActions from '../../redux/actions/interactionActions';
import PropTypes from 'prop-types';
import { newCustomer } from '../../../tools/mockData-cust';
import ManageCustomerForm from './ManageCustomerForm';
import Spinner from '../common/Spinner';
import { toast } from 'react-toastify';

// ** Container for Customer Form **
export function ManageCustomerPage({
  customers,
  interactions,
  loadInteractions,
  loadCustomers,
  saveCustomer,
  history,
  ...props
}) {
  const [customer, setCustomer] = useState({ ...props.customer });
  const [errors, setErrors] = useState();
  const [saving, setSaving] = useState(false);

  useEffect(() => {
    if (customers.length === 0) {
      loadCustomers().catch((error) => {
        alert('Loading customers failed' + error);
      });
    } else {
      setCustomer({ ...props.customer });
    }

    if (interactions.length === 0) {
      loadInteractions().catch((error) => {
        alert('Loading history failed' + error);
      });
    }
  }, [props.customer]); //If empty [] passed as parameter, means only runs once when component mounts.

  function handleChange(event) {
    const { name, value } = event.target;
    setCustomer((prevCustomer) => ({
      ...prevCustomer,
      [name]: name === 'Id' ? parseInt(value, 10) : value,
    }));
  }

  function handleSave(event) {
    event.preventDefault();
    if (!formIsValid()) return;

    setSaving(true);
    saveCustomer(customer)
      .then(() => {
        toast.success('Save Complete');
        history.push('/customers');
      })
      .catch((error) => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
  }

  function formIsValid() {
    const { fname, lname, addressLine } = customer; //client side validation.  Don't be lazy, finish the rest of these later
    const errors = {};

    if (!fname) errors.fname = 'First Name is required';
    if (!lname) errors.lname = 'Last Name is required';
    if (!addressLine) errors.addressLine = 'Address Line is required';
    setErrors(errors);
    return Object.keys(errors).length === 0; //Returns a boolean true if errors.length = 0
  }

  return customers.length === 0 || interactions.length === 0 ? (
    <Spinner />
  ) : (
    <>
      <ManageCustomerForm
        customer={customer}
        errors={errors}
        interactions={getInteractionsByCustomerId(interactions, customer.id)}
        onChange={handleChange}
        onSave={handleSave}
        saving={saving}
      />
    </>
  );
}

ManageCustomerPage.propTypes = {
  customer: PropTypes.object.isRequired,
  interactions: PropTypes.array.isRequired,
  customers: PropTypes.array,
  loadCustomers: PropTypes.func.isRequired,
  loadInteractions: PropTypes.func.isRequired,
  saveCustomer: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export function getCustomerById(customers, id) {
  return customers.find((customer) => customer.id == id) || null; // id is an int, id is passed in url as a string so cheating to make it work quickly by not doing exact comparison.
}

export function getInteractionsByCustomerId(interactions, id) {
  return interactions.filter(
    (interactionsForCustomer) => interactionsForCustomer.custId == id
  );
}

function mapStateToProps(state, ownProps) {
  const id = ownProps.match.params.id;

  const customer =
    id && state.customers.length > 0
      ? getCustomerById(state.customers, id)
      : newCustomer;

  return {
    customer: customer,
    customers: state.customers,
    interactions: state.interactions,
  };
}

const mapDispatchToProps = {
  loadCustomers: customerActions.loadCustomers,
  saveCustomer: customerActions.saveCustomer,
  loadInteractions: interactionActions.loadInteractions,
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageCustomerPage);
