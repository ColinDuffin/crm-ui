import { parseAsync } from '@babel/core';
import http from '../http-common';

export function getCustomers() {
  const responseData = http.get('/customers/').then((res) => res.data);
  return responseData;
}

export async function saveCustomer(customer) {
  if (customer.id) {
    //Update Existing
    return await http.put(
      '/customers/' + customer.id,
      JSON.stringify(customer)
    );
  } else {
    //Create New
    return await http.post('/customers/', JSON.stringify(customer));
  }
}

export async function deleteCustomer(customerId) {
  return await http.delete('/customers/' + customerId);
}
