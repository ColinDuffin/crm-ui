import { combineReducers } from "redux";
import customers from "./customerReducer";
import interactions from "./interactionReducer";
import apiCallsInProgress from "./apiStatusReducer";

const rootReducer = combineReducers({
  customers,
  interactions,
  apiCallsInProgress,
});

export default rootReducer;
