import http from '../http-common';

export function getInteractions() {
  const responseData = http.get('/interactions/').then((res) => res.data);

  return responseData;
}
