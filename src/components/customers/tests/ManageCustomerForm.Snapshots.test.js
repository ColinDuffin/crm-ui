import React from 'react';
import ManageCustomerForm from '../ManageCustomerForm';
import renderer from 'react-test-renderer';
import { customers, interactions } from '../../../../tools/mockData-cust';

it('sets sumbit button to Saving... when saving', () => {
  const tree = renderer.create(
    <ManageCustomerForm
      customer={customers[0]}
      interactions={interactions}
      onSave={jest.fn()}
      onChange={jest.fn()}
      saving
    />
  );
  expect(tree).toMatchSnapshot();
});

it('sets sumbit button to Save when not saving', () => {
  const tree = renderer.create(
    <ManageCustomerForm
      customer={customers[0]}
      interactions={interactions}
      onSave={jest.fn()}
      onChange={jest.fn()}
      saving={false}
    />
  );

  expect(tree).toMatchSnapshot();
});
