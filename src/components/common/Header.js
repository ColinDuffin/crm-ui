import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <nav className="navbar navbar-expand navbar-primary">
      <NavLink to="/" className="nav-link" exact>
        <i className="fas fa-home"></i>
      </NavLink>
      {' | '}
      <NavLink to="/customers" className="nav-link">
        <i className="fas fa-user-friends"></i>
      </NavLink>
      {' | '}
      <NavLink to="/reports" className="nav-link">
        <i className="fas fa-chart-pie"></i>
      </NavLink>
      {' | '}
      <NavLink to="/settings" className="nav-link">
        <i className="fas fa-cog"></i>
      </NavLink>
      {' | '}
      <NavLink to="/about" className="nav-link">
        <i className="fas fa-question-circle"></i>{' '}
      </NavLink>
    </nav>
  );
};

export default Header;
