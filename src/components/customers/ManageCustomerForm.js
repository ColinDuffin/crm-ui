import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../common/TextInput';

const ManageCustomerForm = ({
  customer,
  interactions,
  onSave,
  onChange,
  saving = false,
  errors = {},
}) => {
  return (
    <form onSubmit={onSave}>
      <h2>{customer.id ? 'Customer Details' : 'Add Customer'}</h2>
      {errors.onSave && (
        <div className="alert alert-danger" role="alert">
          {errors.onSave}
        </div>
      )}
      {customer.id && ( //only display ID field for existing customers.  ID should be generated automatically in the backend.
        <TextInput
          name="id"
          label="ID"
          value={customer.id + ''} //cheeky append empty string to the numeric id field causes it to render as a string in the textInput box, cleans a warning from console..
          onChange={onChange}
          error={errors.id}
        />
      )}
      <TextInput
        name="fname"
        label="First Name"
        value={customer.fname}
        onChange={onChange}
        error={errors.fname}
      />
      <TextInput
        name="lname"
        label="Last Name"
        value={customer.lname}
        onChange={onChange}
        error={errors.lname}
      />
      <TextInput
        name="addressLine"
        label="Address Line"
        value={customer.addressLine}
        onChange={onChange}
        error={errors.addressLine}
      />
      <TextInput
        name="city"
        label="City"
        value={customer.city}
        onChange={onChange}
        error={errors.city}
      />
      <TextInput
        name="zip"
        label="Zip"
        value={customer.zip}
        onChange={onChange}
        error={errors.zip}
      />
      <TextInput
        name="email"
        label="Email"
        value={customer.email}
        onChange={onChange}
        error={errors.email}
      />
      <div>
        <label htmlFor="dnsFlag">Do Not Solicit</label>
      </div>
      <div>
        <select
          name="dnsFlag"
          id="dnsFlag"
          value={customer.dnsFlag ? customer.dnsFlag + '' : 'False'}
          onChange={onChange}
          error={errors.dnsFlag}
        >
          <option value="False">False</option>
          <option value="True">True</option>
        </select>
      </div>
      <TextInput
        name="status"
        label="Customer Status"
        value={customer.status}
        onChange={onChange}
        error={errors.status}
      />
      <TextInput
        name="linkedAgent"
        label="Customer's Agent"
        value={customer.linkedAgent}
        onChange={onChange}
        error={errors.linkedAgent}
      />
      <h4>Interaction History</h4>
      <ul className="list-group" key="123">
        <li className="list-group-item">
          <div className="row">
            <div className="col-sm font-weight-bold">ID</div>
            <div className="col-sm font-weight-bold">Customer ID </div>
            <div className="col-sm font-weight-bold">Activity Code</div>
            <div className="col-sm font-weight-bold">Activity Description</div>
            <div className="col-sm font-weight-bold">Activity Timestamp</div>
          </div>
        </li>
        {interactions &&
          interactions.map((interaction, index) => (
            <li className="list-group-item" key={index}>
              <div className="row">
                <div className="col-sm">{interaction.id} </div>
                <div className="col-sm">{interaction.custId} </div>
                <div className="col-sm">{interaction.activityCode} </div>
                <div className="col-sm">{interaction.activityDesc} </div>
                <div className="col-sm">{interaction.timestamp} </div>
              </div>
            </li>
          ))}
      </ul>

      <button type="submit" disabled={saving} className="btn btn-primary">
        {saving ? 'Saving...' : 'Save'}
      </button>
    </form>
  );
};

ManageCustomerForm.propTypes = {
  interactions: PropTypes.array.isRequired,
  customer: PropTypes.object.isRequired,
  errors: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default ManageCustomerForm;
